package jp.co.unirita.da.ml;

import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by Kazuki Niimura on 2015/08/19.
 * Updated by Kazuki Niimura on 2015/08/19
 */
class ModelImport {
    private Classifier classModel;
    private String classModelFile = "src\\main\\resources\\iris.model";

    public ModelImport() throws Exception {
        classModel = (Classifier) weka.core.SerializationHelper.read(classModelFile);
    }

    public void close() {
        classModel = null;
        classModelFile = null;
    }

    public String classifySpecies(Hashtable<String, String> measures) throws Exception {
        FastVector dataClasses = new FastVector();
        FastVector dataAttribs = new FastVector();
        Attribute species;
        double values[] = new double[measures.size() + 1];
        int i = 0, maxIndex = 0;

        //  "dataClasses"に想定しうるアヤメの種別をセット
        dataClasses.addElement("Iris-setosa");
        dataClasses.addElement("Iris-versicolor");
        dataClasses.addElement("Iris-virginica");
        species = new Attribute("species", dataClasses);
        
        //  Create the object to classify on.
        for (Enumeration<String> keys = measures.keys(); keys.hasMoreElements(); ) {
            String key = keys.nextElement();
            double val = Double.parseDouble(measures.get(key));
            dataAttribs.addElement(new Attribute(key));
            values[i++] = val;
        }
        dataAttribs.addElement(species);
        Instances dataModel = new Instances("classify", dataAttribs, 0);
        dataModel.setClass(species);
        dataModel.add(new Instance(1, values));
        dataModel.instance(0).setClassMissing();

        //  Find the class with the highest estimated likelihood

        double cl[] = classModel.distributionForInstance(dataModel.instance(0));
        for(i = 0; i < cl.length; i++)
            if(cl[i] > cl[maxIndex])
                maxIndex = i;

        return dataModel.classAttribute().value(maxIndex);
    }
}
