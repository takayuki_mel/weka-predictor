package jp.co.unirita.da.ml;

import java.util.Hashtable;

/**
 * Created by Kazuki Niimura on 2015/08/19.
 * Updated by Kazuki Niimura on 2015/08/19
 */
public class WekaPrediction {
    public WekaPrediction() {}

    public static void main(String args[]) throws Exception {
        ModelImport model = new ModelImport();
        Hashtable<String, String> values = new Hashtable<>();

        for(String str : args ){
            String[] tokens = str.split("=");
            values.put(tokens[0], tokens[1]);
        }

        System.out.println("Classification: " + model.classifySpecies(values));
    }
}
