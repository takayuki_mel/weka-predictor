package jp.co.unirita.da.ml;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;

import java.util.Hashtable;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;


/**
 * Created by Kazuki Niimura on 2015/08/19.
 * Updated by Kazuki Niimura on 2015/08/19
 */
public class WekaExecutionTest extends TestCase {
    private ModelImport test;

    @Before
    public void setUp() throws Exception {
        try {
            test = new ModelImport();
        } catch (Exception e){
            fail("ModelImportクラスの読み込みに失敗しました: " + e.getMessage());
        }
    }
    
    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        test.close();
    }

    /**
     * Test method for {@link ModelImport#classifySpecies}.
     * @throws Exception
     */
    public void testClassifyIsSetosa() throws Exception {
        Hashtable<String, String> testValues = new Hashtable<>();

        testValues.put("PetalLength", "1.23");
        testValues.put("PetalWidth", "0.11");
        assertThat(test.classifySpecies(testValues), is("Iris-setosa"));
        testValues.clear();

        testValues.put("PetalLength", "1.59");
        testValues.put("PetalWidth", "0.15");
        assertThat(test.classifySpecies(testValues), is("Iris-setosa"));
        testValues.clear();
    }

    public void testClassifyIsVersicolor() throws Exception {
        Hashtable<String, String> testValues = new Hashtable<>();

        testValues.put("PetalLength", "4.25");
        testValues.put("PetalWidth", "1.50");
        assertThat(test.classifySpecies(testValues), is("Iris-versicolor"));
        testValues.clear();

        testValues.put("PetalLength", "4.88");
        testValues.put("PetalWidth", "1.52");
        assertThat(test.classifySpecies(testValues), is("Iris-versicolor"));
        testValues.clear();
    }

    public void testClassifyIsVirginica() throws Exception {
        Hashtable<String, String> testValues = new Hashtable<>();

        testValues.put("PetalLength", "4.21");
        testValues.put("PetalWidth", "1.74");
        assertThat(test.classifySpecies(testValues), is("Iris-versicolor"));
//        assertThat(test.classifySpecies(testValues), is("Iris-virginica"));
        testValues.clear();

        testValues.put("PetalLength", "5.84");
        testValues.put("PetalWidth", "2.01");
        assertThat(test.classifySpecies(testValues), is("Iris-virginica"));
        testValues.clear();
    }
}