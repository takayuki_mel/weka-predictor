# weka-predictor

This project is sample code of Weka using java program.  
This project focus to use that Weka generated classification models.

## Document

[Weka 3: Data Mining Software in Java](http://www.cs.waikato.ac.nz/ml/weka/)  
[Weka - JavaDoc](http://weka.sourceforge.net/doc.stable/)  
[Create a simple predictive analytics classification model in Java with Weka - IBM developerWorks](http://www.ibm.com/developerworks/library/bd-javaweka/)  
[Wekaの簡単な使い方 - グレーゲーム](http://d.hatena.ne.jp/blankblank/20090415/1239806232)